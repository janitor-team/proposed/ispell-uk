# Files that are used for bulding a dictionary
#
# This file was introduced just to have single locations of file names
# for scripts is bin/group* and Dictionary/Makefile
# --dk
#
INDICT=base.lst base-compound.lst names.lst computer.lst dstu_kds.lst geography.lst lang.lst colors.lst base-abbr.lst exceptions.lst pronouns.lst slang.lst
NOTINDICT=""

SORTED=base.lst base-compound.lst names.lst computer.lst geography.lst colors.lst base-abbr.lst slang.lst twisters.lst verify.lst tags.lst rare.lst
NOTSORTED=exceptions.lst pronouns.lst

TAGGED=base.lst base-compound.lst base-abbr.lst computer.lst geography.lst pronouns.lst names.lst dstu_kds.lst slang.lst
SPACED=exceptions.lst lang.lst
NOTSPACED=colors.lst
